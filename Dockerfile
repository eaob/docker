FROM nanoninja/php-fpm

RUN  mkdir -p /speedtest/ \
     && echo "clear_env = no" >> /usr/local/etc/php-fpm.d/www.conf
     
#docker-php-ext-install pdo pdo_mysql 
COPY backend/ /speedtest/backend

COPY results/*.php /speedtest/results/
COPY results/*.ttf /speedtest/results/

COPY *.js /speedtest/
COPY favicon.ico /speedtest/

COPY docker/*.php /speedtest/
COPY docker/entrypoint.sh /

WORKDIR /var/www/html
EXPOSE 9000
CMD ["bash", "/entrypoint.sh"]
