<?php

$db_type=getenv("db_type"); //Type of db: "mysql", "sqlite" or "postgresql"
$stats_password=getenv("stats_password"); //password to login to stats.php. Change $
$enable_id_obfuscation=true; //if set to true, test IDs will be obfuscated to preve$
$redact_ip_addresses=false; //if set to true, IP addresses will be redacted from IP$

// Sqlite3 settings
$Sqlite_db_file = "../../speedtest_telemetry.sql";

// Mysql settings
$MySql_username=getenv('MySql_username');
$MySql_password=getenv('MySql_password');
$MySql_hostname=getenv('MySql_hostname');
$MySql_databasename=getenv('MySql_databasename');

// Postgresql settings
$PostgreSql_username="USERNAME";
$PostgreSql_password="PASSWORD";
$PostgreSql_hostname="DB_HOSTNAME";
$PostgreSql_databasename="DB_NAME";


//IMPORTANT: DO NOT ADD ANYTHING BELOW THIS PHP CLOSING TAG, NOT EVEN EMPTY LINES!
?>
